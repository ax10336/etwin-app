import m from "mithril";

interface EtwinLink {
    (): m.Component<{ to: string; disabled?: boolean }>;
    onclick: null | ((url: string, newTab: boolean) => void);
}

export const EtwinLink: EtwinLink = (): m.Component<{
    to: string;
    disabled?: boolean;
}> => ({
    view: ({ attrs, children }) =>
        m(
            "a",
            {
                class: attrs.disabled
                    ? "etwin-fg-red"
                    : "etwin-fg-soft-purple hover:underline",
                href: attrs.to,
                onclick: (ev: MouseEvent) => {
                    if (ev.button < 2 && EtwinLink.onclick) {
                        const newTab = ev.ctrlKey || ev.button === 1;
                        EtwinLink.onclick(attrs.to, newTab);
                        ev.preventDefault();
                    }
                },
            },
            children,
        ),
});

// act as a normal <a> by default
EtwinLink.onclick = null;
