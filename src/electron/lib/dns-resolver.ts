interface DnsRecordA {
  domain: string;
  type: "A";
  target: string;
}
interface DnsRecordAAAA {
  domain: string;
  type: "AAAA";
  target: string;
}
interface DnsRecordCNAME {
  domain: string;
  type: "CNAME";
  target: string;
}
declare type DnsRecord = DnsRecordA | DnsRecordAAAA | DnsRecordCNAME;

type Domain = string;
type IpV4Addr = string;

export class DnsResolver {
  #resolved: Map<Domain, IpV4Addr>;

  private constructor(resolved: Map<Domain, IpV4Addr>) {
    this.#resolved = resolved;
  }

  public static fromRecords(records: Iterable<Readonly<DnsRecord>>): DnsResolver {
    const targets: Map<Domain, DnsRecordA | DnsRecordCNAME> = new Map();
    for (const record of records) {
      switch (record.type) {
        case "A": {
          targets.set(record.domain, record);
          break;
        }
        case "CNAME": {
          let old = targets.get(record.domain);
          if (old !== undefined) {
            throw new Error("DuplicateDnsTarget");
          }
          targets.set(record.domain, record);
          break;
        }
        case "AAAA": {
          break;
        }
        default: {
          throw new Error("AssertionError: Unexpected DNS record type");
        }
      }
    }

    const resolved: Map<Domain, IpV4Addr> = new Map();
    outer:
      for (const [domain, target] of targets) {
        let cur: DnsRecordA | DnsRecordCNAME | undefined = target;
        while (cur !== undefined) {
          if (cur.type === "A") {
            resolved.set(domain, cur.target);
            continue outer;
          }
          cur = targets.get(cur.target);
        }
      }

    return new DnsResolver(resolved);
  }

  public getAll(): IterableIterator<[Domain,  IpV4Addr]> {
    return this.#resolved[Symbol.iterator]();
  }
}
