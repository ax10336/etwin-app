import type { GameGroup } from "../common/etwinApi";

export type MainMessage =
    | ["data:initialUrl", { initialUrl: string }]
    | ["data:games", { games: GameGroup[] }]
    | ["data:update", { newVersion: string }]
    | ["tab:favicons", { id: string; favicons: string[] }]
    | ["tab:loading", { id: string }]
    | ["tab:stoploading", { id: string }]
    | ["tab:url", { id: string; url: string }]
    | ["tab:title", { id: string; title: string }]
    | ["tab:history", { id: string; canGoBack: boolean; canGoForward: boolean }]
    | ["keybind:newtab", null | { background: boolean; url: string }]
    | ["keybind:closeactivetab", null]
    | ["keybind:tabright", null]
    | ["keybind:tableft", null]
    | ["keybind:focusinput", null];

export type RendererMessage =
    | ["tab:new", { id: string; url: string }]
    | ["tab:show", { id: string }]
    | ["tab:goto", { id: string; url: string }]
    | ["tab:close", { id: string }]
    | ["tab:reload", { id: string }]
    | ["tab:back", { id: string }]
    | ["tab:forward", { id: string }]
    | ["tab:openDevTools", { id: string }]
    | ["tab:zoomIn", { id: string }]
    | ["tab:zoomOut", { id: string }]
    | ["tab:zoomReset", { id: string }];
