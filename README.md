# Eternaltwin Application

## Installation

## Contributing

### Dependencies

- Node 12.18.3
- Yarn 1.22

### Getting started

```
yarn install
yarn start
```

### Scripts

#### `yarn start`

Production build, and then run the app.

#### `yarn build`

Production build.

#### `yarn dev`

Starts the application with live-reload.

#### `yarn dev:chrome`

Chrome-only development. View the Chrome at <http://localhost:3000>.

#### `yarn dist:<platform>`

Create a packaged distribution build for the corresponding platform.

## Versions

The Eternaltwin application bundles:
- Electron 11.5.0
- Chrome 87.0.4280.141
- Node 12.18.3
- Shockwave Flash 32.0
